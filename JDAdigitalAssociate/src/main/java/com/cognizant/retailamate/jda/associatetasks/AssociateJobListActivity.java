package com.cognizant.retailamate.jda.associatetasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cognizant.retailamate.jda.Network.VolleyHelper;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 452781 on 11/28/2016.
 */
public class AssociateJobListActivity extends AppCompatActivity {

    private List<AssociateJobModel.ResourceBean> jobList = new ArrayList<>();
    public RecyclerView recyclerView;
    public AssociateJobAdapter mAdapter;
    private List<AssociateOrderModel> orderModel = new ArrayList<>();
    Gson gson;
    ProgressDialog progress;


    AssociateJobModel associateJobModel;
//    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.associate_job_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gson = new Gson();


        recyclerView = (RecyclerView) findViewById(R.id.associate_job_recyc);

        mAdapter = new AssociateJobAdapter(getApplicationContext(), orderModel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if (AccountState.getOfflineMode()) {
            setJobOffline();
        } else if (!AccountState.getOfflineMode()) {
//            setJobData();
            setJobOffline();
        }
    }


    private void setJobData() {

        progress = ProgressDialog.show(AssociateJobListActivity.this, "Connecting",
                "Please wait...", true);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.getAssociateJobURL + AccountState.getPrefUserId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String object) {
//                        Toast.makeText(AssociateProfileActivity.this, s, Toast.LENGTH_LONG).show();
                        progress.dismiss();
//        String object = loadJSONFromAsset();
                        associateJobModel = gson.fromJson(object, AssociateJobModel.class);
//                        AssociateJobModelold.ResourceBean resourceBean=new AssociateJobModelold.ResourceBean();
//                        resourceBean.setItemName("Order Picking");
//                        resourceBean.setLocation("#1243");
//                        resourceBean.setType("Products");
//                        resourceBean.setQuantity(4);
//                        jobList.add(resourceBean);
                        for (int i = 0; i < associateJobModel.getResource().size(); i++) {
                            jobList.add(associateJobModel.getResource().get(i));
                        }
                        AssociateOrderModel orderModelObj = null;
                        try {
                            orderModelObj = new AssociateOrderModel("Order Picking", jobList.get(0).getOrderNum(), jobList.size(),
                                    "Products", jobList);

                        } catch (IndexOutOfBoundsException | NullPointerException e) {

                        }

                        AssociateOrderModel orderModelObj1 = new AssociateOrderModel("Product Refill", "Product #22123", 0,
                                "Shelf #C2", null);
                        AssociateOrderModel orderModelObj2 = new AssociateOrderModel("Empty Garbage", "Every 5 hours", 0,
                                "", null);
                        orderModel.add(orderModelObj);
                        orderModel.add(orderModelObj1);
                        orderModel.add(orderModelObj2);
                        mAdapter.notifyDataSetChanged();


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dismissing the progress dialog
                        progress.dismiss();
                        Toast.makeText(AssociateJobListActivity.this, error.toString(), Toast.LENGTH_LONG).show();

                    }

                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyHelper.getInstance(AssociateJobListActivity.this).addToRequestQueue(stringRequest);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("AssociateJob.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /*
    OFFLINE mode function for getting associate tasks
     */

    private void setJobOffline() {
        associateJobModel = gson.fromJson(getAssociateJobResponse(), AssociateJobModel.class);
//                        AssociateJobModelold.ResourceBean resourceBean=new AssociateJobModelold.ResourceBean();
//                        resourceBean.setItemName("Order Picking");
//                        resourceBean.setLocation("#1243");
//                        resourceBean.setType("Products");
//                        resourceBean.setQuantity(4);
//                        jobList.add(resourceBean);
        for (int i = 0; i < associateJobModel.getResource().size(); i++) {
            jobList.add(associateJobModel.getResource().get(i));
        }
        AssociateOrderModel orderModelObj = null;
        try {
            orderModelObj = new AssociateOrderModel("Order Picking", jobList.get(0).getOrderNum(), jobList.size(),
                    "Products", jobList);

        } catch (IndexOutOfBoundsException | NullPointerException e) {

        }

        AssociateOrderModel orderModelObj1 = new AssociateOrderModel("Product Refill", "Product #22123", 0,
                "Shelf #C2", null);
        AssociateOrderModel orderModelObj2 = new AssociateOrderModel("Empty Garbage", "Every 5 hours", 0,
                "", null);
        orderModel.add(orderModelObj);
        orderModel.add(orderModelObj1);
        orderModel.add(orderModelObj2);
        mAdapter.notifyDataSetChanged();
    }

    public String getAssociateJobResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("AssociateJob.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}