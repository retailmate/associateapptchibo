package com.cognizant.retailamate.jda.associate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;

public class CustomerInBound extends AppCompatActivity {

    TextView showLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_inbound_popup);

        showLocation = (TextView) findViewById(R.id.show_location);

        showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerInBound.this, CustomerIndoorLocation.class);
                startActivity(intent);


            }
        });
    }


}
