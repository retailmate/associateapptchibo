package com.cognizant.retailamate.jda.associatetasks;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailamate.jda.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDetails extends AppCompatActivity {

    public static List<AssociateJobModel.ResourceBean> jobList;

    RecyclerView recyclerView;
    public static RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    BufferedReader reader = null;
    static Button button;

    Gson gson;

    TextView products, store_id, total_products;

    int products_count;
    String subtaskCount, orderID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        products = (TextView) findViewById(R.id.products);
        store_id = (TextView) findViewById(R.id.store_id);
        total_products = (TextView) findViewById(R.id.total_products);


        gson = new Gson();
        jobList = gson.fromJson(getIntent().getStringExtra("jobList"), new TypeToken<List<AssociateJobModel.ResourceBean>>() {
        }.getType());

        subtaskCount = getIntent().getStringExtra("subtasksCount");
        orderID = getIntent().getStringExtra("orderID");

        products.setText("Sub-Tasks: " + subtaskCount);
        store_id.setText(orderID);
//        total_products.setText(String.valueOf(Integer.getInteger(subtaskCount) / 2));

        total_products.setText(String.valueOf(product_count()));

//        jobList = (List<AssociateJobModelold.ResourceBean>) getIntent().getSerializableExtra("jobListModel");
        button = (Button) findViewById(R.id.button);
        button.setEnabled(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Order has been Completed", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new ProductAdapter(getApplicationContext(), jobList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }


    public void setvisible() {
        button.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.aboutusmenu, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Product count function
     **/
    public int product_count() {

        HashMap<String, Integer> hashMapPick = new HashMap<>();  //hashmap for pick items
        HashMap<String, Integer> hashMapPut = new HashMap<>();  //hashmap for put items
        products_count = 0;
        int listSize = jobList.size();

        for (int i = 0; i < listSize; i++) {
            if (!(jobList.get(i).getItemName() == null)) {                      //ignore null values
                if (jobList.get(i).getType().equals("pick")) {                  //to check for pick
                    if (hashMapPick.containsKey(jobList.get(i).getItemName())) {  //to check for duplicate values
                        Object value = hashMapPick.get(jobList.get(i).getItemName());
                        hashMapPick.put(jobList.get(i).getItemName(), Integer.parseInt(value.toString()) + jobList.get(i).getQuantity()); //if exists add the value
                    } else {
                        hashMapPick.put(jobList.get(i).getItemName(), jobList.get(i).getQuantity());
                    }
                } else if (jobList.get(i).getType().equals("put")) {  //to check for put
                    if (hashMapPut.containsKey(jobList.get(i).getItemName())) {     //to check for duplicate values
                        Object value = hashMapPut.get(jobList.get(i).getItemName());
                        hashMapPut.put(jobList.get(i).getItemName(), Integer.parseInt(value.toString()) + jobList.get(i).getQuantity()); //if exists add the value
                    } else {
                        hashMapPut.put(jobList.get(i).getItemName(), jobList.get(i).getQuantity());
                    }
                }
            }
        }

        HashMap<String, Integer> hashMap = new HashMap<>();  // combined hashmap
        hashMap.putAll(hashMapPick);
        hashMap.putAll(hashMapPut);

        for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
            products_count += entry.getValue();
        }

        return products_count;
    }

}