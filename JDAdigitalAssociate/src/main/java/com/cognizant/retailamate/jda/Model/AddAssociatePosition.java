package com.cognizant.retailamate.jda.Model;

import java.util.List;

/**
 * Created by 452781 on 11/22/2016.
 */
public class AddAssociatePosition {

    /**
     * H : HubServer
     * M : BroadCastMessage
     * A : ["{\"AssociateId\":\"1000389\",\"BeaconId\":\"bdb230bd-0aee-dbe6-ded4-6b3117308b5a\",\"CaptureTime\":\"2016-11-22T07:53:06\"}"]
     */

    private String H;
    private String M;
    private List<String> A;

    public String getH() {
        return H;
    }

    public void setH(String H) {
        this.H = H;
    }

    public String getM() {
        return M;
    }

    public void setM(String M) {
        this.M = M;
    }

    public List<String> getA() {
        return A;
    }

    public void setA(List<String> A) {
        this.A = A;
    }
}
