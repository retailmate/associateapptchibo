package com.cognizant.retailamate.jda.buddystore.interfaces;

/**
 * Created by Bharath on 24/03/17.
 */

public interface ProductInterface {

    void enableButton();
    void disableButton();
    void setIfDisabled();
}
