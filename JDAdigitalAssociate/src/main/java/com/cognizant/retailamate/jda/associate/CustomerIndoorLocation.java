package com.cognizant.retailamate.jda.associate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.cognizant.retailamate.jda.R;

/**
 * Created by 452781 on 4/19/2017.
 */
public class CustomerIndoorLocation extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_indoorlocation);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
