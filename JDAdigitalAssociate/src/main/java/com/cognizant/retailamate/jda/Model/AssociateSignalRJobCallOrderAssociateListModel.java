package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 12/12/2016.
 */

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssociateSignalRJobCallOrderAssociateListModel {

    @SerializedName("OrderNum")
    @Expose
    private Integer orderNum;
    @SerializedName("TaskAssignedTo")
    @Expose
    private ArrayList<AssociateSignalRJobCallModel> taskAssignedTo = new ArrayList<AssociateSignalRJobCallModel>();

    /**
     * @return The orderNum
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * @param orderNum The OrderNum
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * @return The taskAssignedTo
     */
    public ArrayList<AssociateSignalRJobCallModel> getTaskAssignedTo() {
        return taskAssignedTo;
    }

    /**
     * @param resource The taskAssignedTo
     */
    public void setTaskAssignedTo(ArrayList<AssociateSignalRJobCallModel> resource) {
        this.taskAssignedTo = resource;
    }

}