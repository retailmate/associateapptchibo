package com.cognizant.retailamate.jda.buddystore.model;

import java.util.List;

/**
 * Created by Bharath on 24/03/17.
 */

public class Store {
    /**
     * Name : Pepe Men Navy
     * ID : #1246443
     * Category : Men Dept.
     * Stores : [{"Name":"Max Store \u2013 Emirates Mall D3","id":"LOC3232213","Distance":"07 mts"},{"Name":"Max Store \u2013 Dubai Mall A1","id":"LOC376129","Distance":"21 mts"}]
     */

    private String Name;
    private String ID;
    private String Category;
    private List<StoresBean> Stores;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public List<StoresBean> getStores() {
        return Stores;
    }

    public void setStores(List<StoresBean> Stores) {
        this.Stores = Stores;
    }

    public static class StoresBean {
        /**
         * Name : Max Store – Emirates Mall D3
         * id : LOC3232213
         * Distance : 07 mts
         */

        private String Name;
        private String id;
        private String Distance;

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }
    }
}
