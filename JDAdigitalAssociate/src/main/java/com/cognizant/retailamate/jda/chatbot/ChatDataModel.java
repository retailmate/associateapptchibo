package com.cognizant.retailamate.jda.chatbot;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class ChatDataModel {
    String mDataset;
    String mTime;
    Integer mDatasetTypes;




    public String getmDataset() {
        return mDataset;
    }

    public void setmDataset(String mDataset) {
        this.mDataset = mDataset;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public Integer getmDatasetTypes() {
        return mDatasetTypes;
    }

    public void setmDatasetTypes(Integer mDatasetTypes) {
        this.mDatasetTypes = mDatasetTypes;
    }



}
