package com.cognizant.retailamate.jda.buddystore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.interfaces.StoreInterface;
import com.cognizant.retailamate.jda.buddystore.model.Store;

import java.util.List;

/**
 * Created by Bharath on 24/03/17.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {

    private List<Store.StoresBean> storesList;
    Context mContext;

    StoreInterface storeInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView storeName, storeId, storeDistance, viewDetails;


        public MyViewHolder(View view) {
            super(view);
            storeName = (TextView) view.findViewById(R.id.store_name);
            storeId = (TextView) view.findViewById(R.id.store_id);
            storeDistance = (TextView) view.findViewById(R.id.store_distance);
            viewDetails = (TextView) view.findViewById(R.id.view_details);

        }
    }


    public StoreAdapter(List<Store.StoresBean> storesList, Context mContext, StoreInterface storeInterface) {
        this.storesList = storesList;
        this.mContext = mContext;
        this.storeInterface = storeInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.buddy_stores_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Store.StoresBean store = storesList.get(position);
        holder.storeName.setText(store.getName());
        holder.storeId.setText(store.getId());
        holder.storeDistance.setText(store.getDistance());
        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeInterface.startActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return storesList.size();
    }
}