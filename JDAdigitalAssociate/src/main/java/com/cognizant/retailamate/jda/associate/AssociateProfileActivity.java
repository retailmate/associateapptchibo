package com.cognizant.retailamate.jda.associate;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cognizant.retailamate.jda.Model.AssociateDetailsModel;
import com.cognizant.retailamate.jda.Network.VolleyHelper;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.associatetasks.AssociateJobAdapter;
import com.cognizant.retailamate.jda.associatetasks.AssociateJobModel;
import com.cognizant.retailamate.jda.associatetasks.AssociateOrderModel;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by 452781 on 11/22/2016.
 */
public class AssociateProfileActivity extends AppCompatActivity {

    private static final String TAG = AssociateProfileActivity.class.getSimpleName();
    ProgressDialog progress;
    String associateID;
    Gson gson;
    private List<AssociateJobModel.ResourceBean> jobList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AssociateJobAdapter mAdapter;
    AssociateJobModel associateJobModel;

    ImageView imageView;

    private List<AssociateOrderModel> orderModel = new ArrayList<>();

    public static Boolean fromManagerClass = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.associate_details);
        gson = new Gson();
        String jsonString = getIntent().getStringExtra("associate");
        AssociateDetailsModel associateDetailsModel = gson.fromJson(jsonString, AssociateDetailsModel.class);
        Log.e(TAG, jsonString);

        fromManagerClass = getIntent().getBooleanExtra("manager", false);

        associateID = associateDetailsModel.getAssociateID();

        System.out.println("@@## ASSOCIATE ID PROFILE " + associateID);

        TextView associateName = (TextView) findViewById(R.id.associate_profile_name);
        TextView associateZone = (TextView) findViewById(R.id.associate_profile_zone);
        TextView associateDesignation = (TextView) findViewById(R.id.associate_profile_job);
        TextView associateTime = (TextView) findViewById(R.id.associate_profile_time);
        imageView = (ImageView) findViewById(R.id.imageView);


        associateName.setText(associateDetailsModel.getName());
        associateZone.setText(associateDetailsModel.getZone());
        associateDesignation.setText("Cashier");
//        associateID = String.valueOf(4);
        imageView.setImageResource(getImageofAssociate());

        try {

            if (associateDetailsModel.getTime().contains("AM")) {
                associateTime.setText(associateDetailsModel.getTime());
            } else {
                String[] time = new String[2];

                StringTokenizer st = new StringTokenizer(associateDetailsModel.getTime(), " - ");
                int i = 0;
                while (st.hasMoreTokens()) {
                    time[i] = st.nextToken();
                    i++;
                }
                SimpleDateFormat date12Format = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat date24Format = new SimpleDateFormat("hh:mm a");
                try {
                    String startTime = ((date24Format.format(date12Format.parse(time[0]))));
                    String endTime = ((date24Format.format(date12Format.parse(time[1]))));
                    associateTime.setText(startTime + "-" + endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (NullPointerException e) {

        }

        progress = ProgressDialog.show(AssociateProfileActivity.this, "Connecting",
                "Please wait...", true);


        recyclerView = (RecyclerView) findViewById(R.id.associate_job_recyc);
        mAdapter = new AssociateJobAdapter(getApplicationContext(), orderModel);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        /*
        Changed as there is not connection with the task management system
         */

        if (true) {
            setJobOffline();
            progress.dismiss();

        } else {


            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.getAssociateJobURL + associateID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String object) {
                            try {
//                        Toast.makeText(AssociateProfileActivity.this, s, Toast.LENGTH_LONG).show();
                                progress.dismiss();
                                Log.e("999", object);
//                                associateJobModel = gson.fromJson(object, AssociateJobModel.class);

                             /*   AssociateJobModel.ResourceBean resourceBean = new AssociateJobModel.ResourceBean();
                                resourceBean.setItemName("Order Picking");
                                resourceBean.setLocation("#1243");
                                resourceBean.setType("Products");
                                resourceBean.setQuantity(4);
                                jobList.add(resourceBean);*/

                                for (int i = 0; i < associateJobModel.getResource().size(); i++) {
                                    jobList.add(associateJobModel.getResource().get(i));
                                }
                                AssociateOrderModel orderModelObj = new AssociateOrderModel("Order Picking", jobList.get(0).getOrderNum(), jobList.size(),
                                        "Products", jobList);
                                AssociateOrderModel orderModelObj1 = new AssociateOrderModel("Product Refill", "Product #22123", 0,
                                        "Shelf #C2", null);
                                AssociateOrderModel orderModelObj2 = new AssociateOrderModel("Empty Garbage", "Every 5 hours", 0,
                                        "", null);
                                orderModel.add(orderModelObj);
                                orderModel.add(orderModelObj1);
                                orderModel.add(orderModelObj2);

                                mAdapter.notifyDataSetChanged();
                            } catch (NullPointerException | IndexOutOfBoundsException e) {

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Dismissing the progress dialog
                            progress.dismiss();

                            Toast.makeText(AssociateProfileActivity.this, "Login failed. Please enter valid credentials.", Toast.LENGTH_LONG).show();
                            Log.e("@@##", "VolleyError = " + error);
                            Log.e("@@##", "VolleyError = " + error.getMessage());

                            Log.e("@@##", "TEST ");
                            try {
                                byte[] byteArray = error.networkResponse.data;

                                String byteArrayStr = new String(byteArray, "UTF-8");
                                Log.e("@@##", "byteArrayStr = " + byteArrayStr);
                            } catch (UnsupportedEncodingException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }

                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyHelper.getInstance(AssociateProfileActivity.this).addToRequestQueue(stringRequest);
            setJobOffline();
        }
    }

    private int getImageofAssociate() {

        if (associateID.equals("1000243")) {
            return R.drawable.brattly;
        } else if (associateID.equals("1000247")) {
            return R.drawable.bush;
        } else {
            return R.drawable.blue_profile;
        }

    }

    @Override
    protected void onDestroy() {
/*
        Intent intent=new Intent(AssociateProfileActivity.this,HeatMapActivity.class);
        intent.putExtra("flag","allAssociates");
        startActivity(intent);*/
        fromManagerClass = false;
        super.onDestroy();
    }

    /*
    OFFLINE Functions for getting associate task details
     */
    private void setJobOffline() {
        associateJobModel = gson.fromJson(getAssociateJobResponse(), AssociateJobModel.class);
//                        AssociateJobModelold.ResourceBean resourceBean=new AssociateJobModelold.ResourceBean();
//                        resourceBean.setItemName("Order Picking");
//                        resourceBean.setLocation("#1243");
//                        resourceBean.setType("Products");
//                        resourceBean.setQuantity(4);
//                        jobList.add(resourceBean);
        for (int i = 0; i < associateJobModel.getResource().size(); i++) {
            jobList.add(associateJobModel.getResource().get(i));
        }
        AssociateOrderModel orderModelObj = null;
        try {
            orderModelObj = new AssociateOrderModel("Order Picking", jobList.get(0).getOrderNum(), jobList.size(),
                    "Products", jobList);

        } catch (IndexOutOfBoundsException | NullPointerException e) {

        }

        AssociateOrderModel orderModelObj1 = new AssociateOrderModel("Product Refill", "Product #22123", 0,
                "Shelf #C2", null);
        AssociateOrderModel orderModelObj2 = new AssociateOrderModel("Empty Garbage", "Every 5 hours", 0,
                "", null);
        orderModel.add(orderModelObj);
        orderModel.add(orderModelObj1);
        orderModel.add(orderModelObj2);
        mAdapter.notifyDataSetChanged();
    }

    public String getAssociateJobResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("AssociateJob.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}