package com.cognizant.retailamate.jda.associatetasks;

import java.util.List;

/**
 * Created by 599654 on 12/8/2016.
 */

public class AssociateOrderModel {

    private String name;
    private String location;
    private  int quantity;
    private  String type;
    private List<AssociateJobModel.ResourceBean> resource;


    public AssociateOrderModel(String name, String location, int quantity, String type,List<AssociateJobModel.ResourceBean> resource) {
        this.name = name;
        this.location = location;
        this.quantity = quantity;
        this.type = type;
        this.resource=resource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<AssociateJobModel.ResourceBean> getDatafromGson() {
        return resource;
    }

    public void setDatafromGson(String datafromGson) {
        this.resource = resource;
    }

}
