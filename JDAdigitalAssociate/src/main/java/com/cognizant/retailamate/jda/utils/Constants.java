package com.cognizant.retailamate.jda.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 452781 on 12/2/2016.
 */
public class Constants {

    public static String arabic = "عربى";
    public static String english = "English";


    public static String azureJDALoginURL = "http://rmethapi.azurewebsites.net/api/JDAServices/GetAssociateDetails?LoginName=";
    public static String azureLoginURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/ValidateUserAPI?userName=";
    public static String getAssociateDetailsURL = "http://rmethapi.azurewebsites.net/api/JDAServices/GetAssociateScheduleAPI?associateId=";
    public static String getAllAssociatesURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/GetAllAssociatesPositionAPI";
    public static String getAllCustomerURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/GetAllCustomerPositionAPI";
    public static String getAssociateJobURL = "http://rmethapi.azurewebsites.net/api/JDAServices/GetAssociateTasksAPI?associateId=";
    public static String updateAssociateTaskURL = "http://rmethapi.azurewebsites.net/api/JDAServices/UpdateTaskAPI?taskId=";

    public static String sendReport = "http://rmlmapi.azurewebsites.net/api/Reports/StoreDetails";

    public static String SignalRURL = "http://rmlmapi.azurewebsites.net/";
    public static String SignalRHubProxy = "LMHubServer";


    public static int productId;

    public static String loadJSONFromAsset(String filename, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
